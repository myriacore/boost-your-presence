(ns boost-your-presence.middleware
  (:require
   [ring.middleware.content-type :refer [wrap-content-type]]
   [ring.middleware.params :refer [wrap-params]]
   [prone.middleware :refer [wrap-exceptions]]
   [ring.middleware.reload :refer [wrap-reload]]
   [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
   [ring.middleware.ssl :refer [wrap-ssl-redirect]]
   [ring.middleware.defaults :refer [site-defaults wrap-defaults]]))

(def middleware
  [#(wrap-defaults % (assoc-in site-defaults [:session :cookie-attrs :same-site] :lax))
   wrap-exceptions
   wrap-reload
   wrap-json-response
   #(wrap-json-body % {:keywords? true})])
   
