(ns boost-your-presence.middleware
  (:require
   [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
   [ring.middleware.ssl :refer [wrap-forwarded-scheme wrap-ssl-redirect]]
   [ring.middleware.defaults :refer [site-defaults wrap-defaults]]))

(def middleware
  [#(wrap-defaults % (assoc-in site-defaults [:session :cookie-attrs :same-site] :lax))
   wrap-forwarded-scheme
   wrap-ssl-redirect
   wrap-json-response
   #(wrap-json-body % {:keywords? true})])
