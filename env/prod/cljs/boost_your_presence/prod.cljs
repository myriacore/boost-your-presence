(ns boost-your-presence.prod
  (:require [boost-your-presence.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
