(ns boost-your-presence.server
    (:require
     [boost-your-presence.handler :refer [app]]
     [config.core :refer [env]]
     [boost-your-presence.db :as db :refer [init!]]
     [ring.adapter.jetty :refer [run-jetty]])
    (:gen-class))

(defn -main [& args]
  (let [port (or (env :port) 3000)]
    (db/init!)
    (run-jetty #'app {:port port :join? false})))
