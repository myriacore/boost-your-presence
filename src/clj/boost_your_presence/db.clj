(ns boost-your-presence.db
    (:require
     [config.core :refer [env]]
     [next.jdbc :as jdbc]
     [next.jdbc.sql :as sql]
     [next.jdbc.quoted :refer [postgres]])
    (:gen-class))

;; -------------------------------
;; DB Connection

(def db-spec
  (cond
    (some? (:jdbc-database-url env)) ;; Heroku will set JDBC_DATABASE_URL
    (:jdbc-database-url env)

    (some? (:db env))
    (:db env)

    :else
    {:dbtype "postgresql"
     :dbname "//localhost/testdb"}))

(def data-source
  (jdbc/get-datasource db-spec))

;; -------------------------------
;; DB Schema

(defn- create-tables! [ds]
  "Creates tables in the given datasource, DS."
  (jdbc/execute! ds
    ["CREATE TABLE IF NOT EXISTS region
      (name VARCHAR NOT NULL, 
       id VARCHAR PRIMARY KEY, 
       icon VARCHAR,
       description VARCHAR);
    CREATE TABLE IF NOT EXISTS location
      (name VARCHAR NOT NULL, 
       id VARCHAR PRIMARY KEY, 
       icon VARCHAR, 
       region VARCHAR NOT NULL, 
       description VARCHAR);
    CREATE TABLE IF NOT EXISTS \"user\"
      (name VARCHAR NOT NULL, 
       id VARCHAR PRIMARY KEY, 
       icon VARCHAR, 
       location VARCHAR NOT NULL, 
       boosting boolean,
       rallying VARCHAR);"]))

;; -------------------------------
;; User

(defn find-user [partial-user]
  "Given a partial map of a user, queries the database for
  the user and returns all the hits."
  (sql/find-by-keys data-source :user 
    (select-keys partial-user [:name :id :icon 
                               :location :boosting
                               :rallying])
    {:table-fn postgres
     :column-fn postgres})) 

(defn get-user-by-id [user-id]
  "Given the user-id of a user, queries the database for
  the user."
  (sql/get-by-id data-source :user user-id
                 {:table-fn postgres
                  :column-fn postgres}))

(defn add-user! [user]
  "Adds a user to the database."
  (sql/insert! data-source :user 
    (as-> user user
      (select-keys user 
        [:name :id :icon 
         :location :boosting 
         :rallying])
      ;; Add defaults. Keep argument-provided entries when collisions occur.
      (merge-with #(%) user
        {:location "TESTLOCID"
         :boosting false
         :rallying "TESTRALLYID"}))
   {:table-fn postgres
    :column-fn postgres}))


(defn update-user! [user where]
  "Updates an existing user within the database.
  The WHERE param can be either {:key val} (which checks for equality),
  or [\"KEY = ?\" val], which just embeds a sql query. "
  (sql/update! data-source :user 
    (select-keys user [:name :id :icon 
                       :location :boosting 
                       :rallying]) 
    where
    {:table-fn postgres
     :column-fn postgres}))


(defn update-user-by-id! [user]
  "updates an existing user within the database based on the user's
  :id key."
  (sql/update! data-source :user 
    (select-keys user [:name :icon 
                       :location :boosting 
                       :rallying]) 
    (select-keys user [:id])
    {:table-fn postgres
     :column-fn postgres}))

;; -------------------------------
;; Location

(defn find-location [partial-location]
  "Given a partial map of a location, queries the database for
  the location and returns all the hits."
  (sql/find-by-keys data-source :location 
    (select-keys partial-location [:todo])
    {:table-fn postgres
     :column-fn postgres}))

(defn get-location-by-id [location-id]
  "Given the location-id of a location, queries the database for
  the location."
  (sql/get-by-id data-source :location location-id 
    {:table-fn postgres
     :column-fn postgres}))

(defn add-location! [location]
  "Adds a location to the database"
  (sql/insert! data-source :location 
    (select-keys location [:todo])
    {:table-fn postgres
     :column-fn postgres}))

(defn update-location! [location where]
  "Updates an existing location within the database.
  The WHERE param can be either {:key val} (which checks for equality),
  or [\"KEY = ?\" val], which just embeds a sql query. "
  (sql/update! data-source :location 
    (select-keys location [:todo]) 
    where
    {:table-fn postgres
     :column-fn postgres}))

(defn update-location-by-id! [location]
  "updates an existing location within the database based on the location's
  :id key."
  (sql/update! data-source :location 
    (select-keys location [:todo]) 
    (select-keys location [:id])
    {:table-fn postgres
     :column-fn postgres}))

;; -------------------------------
;; Region

(defn find-region [partial-region]
  "Given a partial map of a region, queries the database for
  the region and returns all the hits."
  (sql/find-by-keys data-source :region 
    (select-keys partial-region [:todo])
    {:table-fn postgres
     :column-fn postgres}))

(defn get-region-by-id [region-id]
  "Given the region-id of a region, queries the database for
  the region."
  (sql/get-by-id data-source :region region-id 
    {:table-fn postgres
     :column-fn postgres}))

(defn add-region! [region]
  "Adds a region to the database"
  (sql/insert! data-source :region 
    (select-keys region [:todo])
    {:table-fn postgres
     :column-fn postgres}))

(defn update-region! [region where]
  "Updates an existing region within the database.
  The WHERE param can be either {:key val} (which checks for equality),
  or [\"KEY = ?\" val], which just embeds a sql query. "
  (sql/update! data-source :region 
    (select-keys region [:todo]) 
    where
    {:table-fn postgres
     :column-fn postgres}))

(defn update-region-by-id! [region]
  "updates an existing region within the database based on the region's 
  :id key."
  (sql/update! data-source :region 
    (select-keys region [:todo]) 
    (select-keys region [:id])
    {:table-fn postgres
     :column-fn postgres}))

;; -------------------------------
;; Administration / Lifecycle

(defn init! []
  "Initialize Database, assuming database is empty."
  (create-tables! data-source))

