(ns boost-your-presence.handler
  (:require
   [reitit.ring :as reitit-ring]
   [boost-your-presence.middleware :refer [middleware]]
   [boost-your-presence.db :as db]
   [ring.middleware.oauth2 :refer [wrap-oauth2]]
   [clj-http.client :as http]
   [clojure.pprint :refer [pprint]]
   [clojure.data.json :as json]
   [clojure.set :refer [rename-keys]]
   [hiccup.page :refer [include-js include-css html5]]
   [config.core :refer [env]]))

;; ----------------------------------
;; API Functions

(defn call-discord [req-type endpoint access-token]
  "Calls the discord API with the provided oauth2 access token.
  
  See discord documentation for more information on what you'll get back:
  https://discord.com/developers/docs/reference"
  (let [base-url "https://discord.com/api"
        req-url (str base-url endpoint)
        http-opts {:oauth-token access-token :accept :json}
        parse-json #(json/read-str % :key-fn keyword)]
    (case req-type
      :get (-> (http/get req-url http-opts) :body parse-json)
      :post (-> (http/post req-url http-opts) :body parse-json))))

(defn update-db [_request]
  "Given an authenticated user request, uses the access-token
  stored in the request's session to update the database with
  new information from discord. "
  (let [access-token (-> _request :oauth2/access-tokens :discord :token)
        user (call-discord :get "/users/@me" access-token)
        user-exists? #(some-> % :id db/get-user-by-id)
        mk-avatar #(str "https://cdn.discordapp.com/avatars/" %1 "/" %2 ".png")]
    ;; Update User
    (-> user ;; https://discord.com/developers/docs/resources/user
        (rename-keys {:username :name :avatar :icon})
        (update :icon (partial mk-avatar (:id user)))
        (select-keys [:id :name :icon])
        (as-> cleaned-user 
          (if (user-exists? cleaned-user)
            (db/update-user-by-id! cleaned-user)
            (db/add-user! cleaned-user))))))

;; ----------------------------------
;; Static HTML Views

(def mount-target
  [:div#app
   [:h2 "Welcome to boost-your-presence"]
   [:p "please wait while Figwheel is waking up ..."]
   [:p "(Check the js console for hints if nothing exciting happens.)"]])

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1"}]
   (include-css (if (env :dev) "/css/site.css" "/css/site.min.css"))])

(defn loading-page []
  (html5
   (head)
   [:body {:class "body-container"}
    mount-target
    (include-js "/js/app.js")]))

;; ----------------------------------
;; Route Handlers

(defn index-handler [_request]
  (if (-> _request :oauth2/access-tokens :discord :token)
    (update-db _request))
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body (loading-page)})

(defn login-handler [_request]
  "Handles Oauth2 Discord Redirect"
  (update-db _request)
  {:status 307   ;; Redirect to index
   :headers {"Location" "/"}})

(defn logout-handler [_request]
  (if-let [access-token (-> _request :oauth2/access-tokens :discord :token)]
    {:status 302   ;; Redirect to index
     :headers {"Location" "/"}
     :session nil} ;; Invalidate session
    {:status 403
     :headers {"Content-Type" "application/json"}
     :body {:error "Not Logged in!"}})) 

(defn current-user-info-handler [_request]
  "Get information about the currently-logged in user, and send it to client."
  (if-let [access-token (-> _request :oauth2/access-tokens :discord :token)]
    (let [user (db/get-user-by-id (:id (call-discord :get "/users/@me" access-token)))
          servers (call-discord :get "/users/@me/guilds" access-token)
          server-image #(str "https://cdn.discordapp.com/icons/" %1 "/" %2 ".png")]
      {:status 200
       :headers {"Content-Type" "application/json"}
       :body (assoc user :user/servers
                (for [server servers]
                  {:server/name (:name server)
                   :server/icon (server-image (:id server) (:icon server))}))})
    {:status 403
     :headers {"Content-Type" "application/json"}
     :body {:error "Not Logged in!"}}))

;; ----------------------------------
;; Middleware / App

(defn discord-handler [routes]
  (wrap-oauth2
    routes
    {:discord
     {:authorize-uri "https://discord.com/api/oauth2/authorize"
      :access-token-uri "https://discord.com/api/oauth2/token"
      :client-id (:discord-client-id env)
      :client-secret (:discord-client-secret env)
      :scopes ["email", "identify", "guilds"]
      :launch-uri "/login"
      :redirect-uri "/login/callback"
      :redirect-handler login-handler ;; Not supported by [ring-oauth2 "0.1.5"]
      :landing-uri "/"}}))

(def app
  (reitit-ring/ring-handler
   (reitit-ring/router
    [["/" {:get {:handler index-handler}}]
     ["/items"
      ["" {:get {:handler index-handler}}]
      ["/:item-id" {:get {:handler index-handler
                          :parameters {:path {:item-id int?}}}}]]
     ["/about" {:get {:handler index-handler}}]
     ["/login/callback" {:get {:handler #({:status 200})}}]
     ["/logout" {:get {:handler logout-handler}}]
     ["/api/user" {:get {:handler current-user-info-handler}}]])
   (reitit-ring/routes
    (reitit-ring/create-resource-handler {:path "/" :root "/public"})
    (reitit-ring/create-default-handler))
   {:middleware (conj middleware discord-handler)}))
