(ns boost-your-presence.util
  (:require [clojure.walk :refer [postwalk]]))

(defn map-assoc [f m]
  "Runs the function f on each key-value vector pair in the map m"
  ;; only apply to maps
  (postwalk (fn [x] (if (map? x) (into {} (map f x)) x)) m))


